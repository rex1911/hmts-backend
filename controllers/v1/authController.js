const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const UserModel = require("../../models/userModel");

module.exports.login = async (req, res) => {
	try {
		const user = await UserModel.findOne({ username: req.body.username }).exec();
		if (!user) {
			throw "Invalid username or password";
		}
		const match = await bcrypt.compare(req.body.password, user.password);
		if (!match) {
			throw "Invalid username or password";
		}
		const jwtPayload = {
			// eslint-disable-next-line no-underscore-dangle
			id: user._id,
		};
		const token = await jwt.sign(jwtPayload, process.env.JWTSECRET, { expiresIn: "7d" });
		// eslint-disable-next-line no-underscore-dangle
		res.status(200).json({ ...user._doc, token });
	} catch (error) {
		const response = {
			error: true,
			errorMessage: error,
		};
		res.status(500).json(response);
	}
};
