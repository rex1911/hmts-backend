const cryptoRandomString = require("crypto-random-string");
const Device = require("../../models/deviceModel");
const User = require("../../models/userModel");
const sendMail = require("../../utils/mail");

module.exports.verifyDevice = async (req, res) => {
	const { id } = req.params;

	try {
		const device = await Device.findOne({ deviceID: id }).exec();

		if (!device) throw "Device not found";

		res.status(200).json({ exist: true });
	} catch (error) {
		const response = {
			exist: false,
		};
		res.status(404).json(response);
	}
};

module.exports.getDeviceData = async (req, res) => {
	try {
		const userID = req.user.id;
		const user = await User.findById(userID).populate("device").exec();
		const { device } = user;
		res.status(200).json(device);
	} catch (error) {
		const response = {
			error: true,
			message: "Something went wrong",
		};
		res.status(500).json(response);
	}
};

module.exports.updateDevice = async (req, res) => {
	try {
		// Update device
		const { id } = req.params;
		const { temp, pulse, lat, lon } = req.body;
		const device = await Device.findOne({ deviceID: id }).exec();
		if (!device) throw "Not found";
		device.temp = temp;
		device.pulse = pulse;
		device.lat = lat;
		device.lon = lon;
		const updatedDevice = await device.save();

		// Send email
		if (temp > 37 || pulse > 100 || pulse < 60) {
			const currentTime = Date.now();
			//Target time is 10 mins after current time
			const targetTime = currentTime - process.env.MAILTIME;
			const ttDate = new Date(targetTime).toISOString();

			let type;
			if (pulse > 100 || pulse < 60) {
				type = "pulse";
			}
			if (temp > 37) {
				type = "temp";
			}

			const users = await User.find({
				// eslint-disable-next-line no-underscore-dangle
				device: device._id,
				lastAlert: { $lte: ttDate },
			}).exec();
			users.forEach(async (user) => {
				try {
					await sendMail(user, pulse, temp, type);
					const newUser = user;
					newUser.lastAlert = Date.now();
					await newUser.save();
					// eslint-disable-next-line no-console
					console.log(`[EMAIL] Sent email to ${user.relativeEmail}`);
				} catch (err) {
					// eslint-disable-next-line no-console
					console.log(`[EMAIL] Failed to send mail to ${user.relativeEmail}`);
				}
			});
		}

		res.status(200).json({
			success: true,
			device: updatedDevice,
		});
	} catch (err) {
		res.status(500).json({ success: false });
		// eslint-disable-next-line no-console
		console.log(err);
	}
};

module.exports.createRandomDevice = async (req, res) => {
	const randomNumber = (min, max) => Math.random() * (max - min) + min;

	try {
		const deviceID = cryptoRandomString({ length: 6, type: "url-safe" });
		const temp = randomNumber(20, 40);
		const pulse = randomNumber(70, 100);
		const lat = randomNumber(19.177084, 19.176025);
		const lon = randomNumber(72.966761, 72.964519);
		const newDeviceData = { deviceID, temp, pulse, lat, lon };
		const device = new Device(newDeviceData);
		const saved = await device.save();
		res.status(200).json(saved);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};
