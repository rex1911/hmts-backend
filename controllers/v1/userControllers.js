/* eslint-disable no-underscore-dangle */
const { hashPassword } = require("../../utils/utils");
const UserModel = require("../../models/userModel");
const Device = require("../../models/deviceModel");

module.exports.getSingleUser = async (req, res) => {
	try {
		const { id } = req.params;
		if (id !== req.user.id) {
			throw "Something went wrong. Please try again";
		}
		const user = await UserModel.findById(id).exec();
		res.status(200).json(user);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};

module.exports.getAllUsers = async (req, res) => {
	try {
		const users = await UserModel.find({}).exec();
		res.status(200).json(users);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};

module.exports.addUser = async (req, res) => {
	try {
		const hashed = await hashPassword(req.body.password);
		const device = await Device.findOne({ deviceID: req.body.device }).exec();
		const newUser = {
			...req.body,
			password: hashed,
			lastAlert: 0,
			device: device._id,
		};
		const user = new UserModel(newUser);

		const saved = await user.save();
		res.status(200).json(saved);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};

module.exports.updateUser = async (req, res) => {
	const { id } = req.params;
	try {
		if (id !== req.user.id) {
			throw "Something went wrong. Please try again";
		}

		const user = await UserModel.findById(id).exec();

		const fields = Object.keys(req.body);
		fields.forEach(async (field) => {
			if (field === "password") {
				const hashed = await hashPassword(req.body[field]);
				req.body[field] = hashed;
			}

			user[field] = req.body[field];
		});

		const updatedUser = await user.save();
		res.status(200).json(updatedUser);
	} catch (error) {
		const response = {
			error: true,
			errorMessage: "Something went wrong. Please try again.",
		};
		res.status(500).json(response);
	}
};

module.exports.checkUsername = async (req, res) => {
	const { username } = req.params;
	const user = await UserModel.findOne({ username }).exec();
	if (user) {
		res.status(200).json({ available: false });
	} else {
		res.status(200).json({ available: true });
	}
};
