const jwt = require("jsonwebtoken");

const authenticate = async (req, res, next) => {
	const authHeader = req.headers.authorization;

	const response = {
		error: true,
		errorMessage: "Not logged in",
	};

	if (authHeader) {
		try {
			const token = authHeader;
			const decoded = await jwt.verify(token, process.env.JWTSECRET);
			req.user = {};
			req.user.id = decoded.id;
			next();
		} catch (error) {
			res.status(401).json(response);
		}
	} else {
		res.status(401).json(response);
	}
};

module.exports = authenticate;
