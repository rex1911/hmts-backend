const { Router } = require("express");

const router = Router();
const {
	verifyDevice,
	getDeviceData,
	updateDevice,
	createRandomDevice,
} = require("../../controllers/v1/deviceController");
const authenticate = require("../../middleware/auth");

router.get("/device/verify/:id", verifyDevice);
router.get("/device", authenticate, getDeviceData);
router.post("/device", createRandomDevice);
router.post("/device/update/:id", updateDevice);

module.exports = router;
