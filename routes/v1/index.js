const userRoutes = require("./userRoutes");
const authRoutes = require("./authRoutes");
const deviceRoutes = require("./deviceRoutes");

module.exports = {
	userRoutes,
	authRoutes,
	deviceRoutes,
};
