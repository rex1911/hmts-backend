const { Router } = require("express");

const router = Router();

const authenticate = require("../../middleware/auth");

const {
	getSingleUser,
	getAllUsers,
	addUser,
	updateUser,
	checkUsername,
} = require("../../controllers/v1/userControllers");

router.get("/users/:id", authenticate, getSingleUser);
router.get("/users", getAllUsers);
router.post("/users", addUser);
router.put("/users/:id", authenticate, updateUser);
router.get("/users/check-username/:username", checkUsername);

module.exports = router;
