const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	username: String,
	password: String,
	name: String,
	relativeName: String,
	relativeEmail: String,
	address: String,
	gender: String,
	email: String,
	lastAlert: Date,
	device: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "Device",
	},
});

// const encKey = process.env.ENC_KEY;
// const sigKey = process.env.SIG_KEY;

// userSchema.plugin(encrypt, { encryptionKey: encKey,  signingKey: sigKey } )

module.exports = mongoose.model("User", userSchema);
