const mongoose = require("mongoose");
const encrypt = require("mongoose-encryption");

const deviceSchema = mongoose.Schema({
	deviceID: String,
	temp: Number,
	pulse: Number,
	lat: String,
	lon: String,
});

const encKey = process.env.ENC_KEY;
const sigKey = process.env.SIG_KEY;

deviceSchema.plugin(encrypt, { encryptionKey: encKey, signingKey: sigKey, excludeFromEncryption: ["deviceID"] });

module.exports = mongoose.model("Device", deviceSchema);
