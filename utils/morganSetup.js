const rfs = require("rotating-file-stream");
const morgan = require("morgan");
const path = require("path");
const moment = require("moment-timezone");

const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;

const stream = rfs.createStream(() => `${moment().tz(tz).format("DD-MM-YYYY")}.log`, {
	interval: "1d",
	path: path.join(__dirname, "../logs"),
});

// eslint-disable-next-line no-unused-vars
morgan.token("date", (req, res) => moment().tz(tz).format("LLL z"));
morgan.format("custom", ':remote-addr - [:date] ":method :url" :status');

// eslint-disable-next-line no-unused-vars
const morganSkip = (req, res) => {
	const regex = /(\/logs.*)|(\/favicon.ico)/g;
	const url = req.originalUrl;
	const match = url.search(regex);

	if (!(match === -1)) {
		return true;
	}
	return false;
};

module.exports.morgan = morgan;
module.exports.stream = stream;
module.exports.morganSkip = morganSkip;
