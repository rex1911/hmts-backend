const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
	host: "smtp.gmail.com",
	port: 465,
	secure: true,
	auth: {
		type: "OAuth2",
		user: process.env.userEmail,
		clientId: process.env.clientID,
		clientSecret: process.env.clientSecret,
		refreshToken: process.env.refreshToken,
	},
});

const getPulseMail = (user, pulse) => ({
	from: "HMTS <mailer@imageconstructor.iam.gserviceaccount.com>",
	to: user.relativeEmail,
	subject: "HMTS Alert",
	text: `Dear sir/Madam,\r\n    We are from HMTS. While monitoring ${user.name} our system came to know that the pulse of the patient is not stable. \r\nIt is ${pulse}. It is not in normal range that is from 60 to 100. \r\n\r\nCall 108 for Ambulance Assitance`,
});

const getTempMail = (user, temp) => ({
	from: "HMTS <vinaytarle@gmail.com>",
	to: user.relativeEmail,
	subject: "HMTS Alert",
	text: `Dear Sir/Madam,\r\n    We are from HMTS. While monitoring ${user.name} our system came to know that the temperate of the patient is not stable. \r\nIt is ${temp} degree Celsius. It is above 37 degree Celsius which is above normal human body temperature.\r\n\r\nCall 108 for Ambulance.`,
});

const sendMail = (user, pulse, temp, type) =>
	new Promise((resolve, reject) => {
		let mail;
		if (type === "pulse") {
			mail = getPulseMail(user, pulse);
		} else if (type === "temp") {
			mail = getTempMail(user, temp);
		}

		transporter.sendMail(mail, (err, info) => {
			if (err) {
				reject(err);
			} else {
				resolve(info);
			}
		});
	});

module.exports = sendMail;
