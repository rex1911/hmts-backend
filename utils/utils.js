const bcrypt = require("bcrypt");

const SALTROUNDS = 10;

module.exports.hashPassword = (password) =>
	// eslint-disable-next-line no-async-promise-executor
	new Promise(async (resolve, reject) => {
		try {
			const hashed = await bcrypt.hash(password, SALTROUNDS);
			resolve(hashed);
		} catch (error) {
			const response = {
				error: true,
				errorMessage: "Failed to hash password",
			};
			reject(response);
		}
	});
