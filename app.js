require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const serveIndex = require("serve-index");
const { morgan, stream, morganSkip } = require("./utils/morganSetup");

const v1API = require("./routes/v1/index");

const app = express();
//= =======================
// MongoDB setup
//= =======================
mongoose.connect(process.env.MONGO_URI || "mongodb://localhost/hmts", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
});

//=======================
// Middleware
//=======================
app.use(morgan("custom", { stream, skip: morganSkip }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

//======================
//     ROUTES
//======================
app.use("/api/v1", v1API.userRoutes);
app.use("/api/v1", v1API.authRoutes);
app.use("/api/v1", v1API.deviceRoutes);
app.use("/logs", express.static("logs/"), serveIndex("logs/", { icons: true }));
app.get("/", (req, res) => {
	res.send("Serving HMTS API");
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
	// eslint-disable-next-line no-console
	console.log(`App listening on port ${PORT}`);
});
